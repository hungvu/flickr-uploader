package com.hungvu.uploader;

import android.graphics.Rect;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.collections.CollectionsInterface;
import com.googlecode.flickrjandroid.people.User;
import com.hungvu.uploader.CustomUI.ScrimInsetsFrameLayout;

import org.json.JSONException;

import java.io.IOException;

public class UploaderMainActivity extends AppCompatActivity implements ScrimInsetsFrameLayout.OnInsetsCallback {

    private Toolbar toolbar;
    private DrawerLayout mDrawerLayout;
    private String[] mMenuTitles;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    /*******************
     *   Overrides     *
     *******************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploader_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ScrimInsetsFrameLayout scrimInsetsFrameLayout = (ScrimInsetsFrameLayout)
                    findViewById(R.id.scrim_insets_main);
            scrimInsetsFrameLayout.setOnInsetsCallback(this);
        }

        setupToolbar();

        setupNavigationDrawer();

        Flickr flickr = new Flickr(getResources().getString(R.string.flickr_key));
        try {
            User user = flickr.getOAuthInterface().testLogin();
        } catch (FlickrException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_uploader_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInsetsChanged(Rect insets) {
        View topBarGreen = findViewById(R.id.top_bar_main_scr);
        ViewGroup.LayoutParams layoutParams = topBarGreen.getLayoutParams();
        layoutParams.height = insets.top;

    }
    /*****************
     *    Methods    *
     *****************/
    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_main_activity);
        toolbar.setBackgroundColor(getResources().getColor(R.color.blue));
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Uploader");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.appbar_menu);
    }

    private void setupNavigationDrawer() {
        mMenuTitles = getResources().getStringArray(R.array.menus);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        //add header for Nav Drawer
        View v = getLayoutInflater().inflate(R.layout.nav_drawer_header, null, false);
        mDrawerList.addHeaderView(v);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mMenuTitles));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }


}
